import Vue from 'vue'
import Router from 'vue-router'
//import {i18n} from '@/plugins/i18n'
import {Trans} from '@/plugins/Translation'


// load views
function load(component) {
    // '@' is aliased to src/components
    return () => import(/* webpackChunkName: "[request]" */ `@/views/${component}.vue`)
}


Vue.use(Router)

const routes = [
    {
        path: '/',
        name: 'home',
        component: load('Home')
    },

    {
        path: '/legal-notice//',
        name: 'legal_notice_en',
        component: load('static_pages/en/LegalNotice')

    },
    {
        path: '/privacy-policy//',
        name: 'privacy_policy_en',
        component: load('static_pages/en/PrivacyPolicy')

    },

    // {
    //     path: '/:lang',
    //     component: {
    //         template: '<router-view></router-view>'
    //     },
    //     beforeEnter: Trans.routeMiddleware,
    //
    //     children: [
    //         {
    //             path: '',
    //             name: 'home',
    //             component: load('Home')
    //         },
    //
    //
    //     ]
    // },

    // {
    //     // Redirect user to supported lang version.
    //     path: '*',
    //     redirect(to) {
    //
    //         return Trans.getUserSupportedLang() + to.path.replace(/\/$/, "/")
    //     }
    // }


    {
        path: '*',
        component: load('404')
    }
]

const router = new Router({
    mode: 'history',
    //base: process.env.BASE_URL,
    routes,
    scrollBehavior(to, from, savedPosition) {
        // return desired position
//        console.log('scrollBehavior');
        if (!to.hash) {
            return {x: 0, y: 0}
        }

    }
})

export default router
