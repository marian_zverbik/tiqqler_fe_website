import Vue from 'vue'
import App from './App.vue'
import {i18n} from '@/plugins/i18n'
import {Trans} from './plugins/Translation'
import router from './router'


Vue.prototype.$i18nRoute = Trans.i18nRoute.bind(Trans)

import VueMeta from 'vue-meta'

Vue.use(VueMeta, {
  // optional pluginOptions
  refreshOnceOnNavigation: true
})

/* add global env variables to variable $env */
Vue.prototype.$env = process.env

import VueSwal from 'vue-swal'
Vue.use(VueSwal)


Vue.config.productionTip = false

new Vue({
    router,
    i18n,
    render: h => h(App)
}).$mount('#app')



