
# Frontend (app folder)
## vue app

### Instructions for uploading to the production/staging server:

All of the configuration options for the frontend are stored in the .env file and for:


 production in .env.production file

 local in .env.local file. 


####Production server

You have to compile the actual resources with the 'npm run build' command (in app folder) before uploading resources to the production server
```
npm run build
```

You have to upload all from 'dist' subdirectory to the site's root directory on production/staging server

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

###  Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
