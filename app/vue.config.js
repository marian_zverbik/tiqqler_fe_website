module.exports = {



    lintOnSave: true, //warnings are only logged to the terminal and does not fail the compilation, so this is a good default for development.




    chainWebpack: (config) => {
        const svgRule = config.module.rule('svg');

        svgRule.uses.clear();

        config.module
            .rule('vue')
            .use('vue-loader')
            .loader('vue-loader')
            .tap(options => {
                // modify the options...
                return options
            });

        svgRule
            .use('babel-loader')
            .loader('babel-loader')
            .end()
            .oneOf('inline')
            .resourceQuery(/inline/)
            .use('file-loader')
            .loader('file-loader')
            .end()
            .end()
            .oneOf('external')
            .use('vue-svg-loader')
            .loader('vue-svg-loader')
            .options({
                name: 'assets/[name].[hash:8].[ext]',
                svgo: {
                    plugins: [{removeDimensions: true, removeViewBox: false, removeDoctype: true, removeComments: true, cleanupIDs: false}]
                }
            });
    },


     pluginOptions: {

        sitemap: {
             baseURL: 'https://tiqqlerweb.azurewebsites.net',

            urls: [
                 '/',
                // '/en',
                // '/de',
            ],

            pretty: true,
            defaults: {
                lastmod: new Date().toISOString().slice(0, 10),
                changefreq: 'weekly',
                priority: 1.0,
            },
            outputDir: './public',
            trailingSlash: true,
            productionOnly: false,
        }
    },



    runtimeCompiler: true
}
